package prurbasJunit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.itinajero.app.model.Horario;
import net.itinajero.app.model.Pelicula;

public class HorarioTest {
	
	
	public Pelicula peli;
	public Horario horarioValido;
	@Before
	public void SetUp() {
	peli=new Pelicula();
	horarioValido = mock(Horario.class);
	peli.setClasificacion("R");
	peli.setDuracion(200);
	peli.setTitulo("poio");
	peli.setGenero("comedia");
	peli.setImagen("asdsd");
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    String dateInString = "7-Jun-2013";
    Date date;
	try {
		date = formatter.parse(dateInString);
		peli.setFechaEstreno(date);
	} catch (ParseException e) {
		e.printStackTrace();
	}

	peli.setEstatus("Activa");
	peli.setId(1000);
	peli.setDetalle(null);
	}
	@After
	@Test
	public void testSetHorarioValido()
	{
		horarioValido.setPelicula(peli);
		verify(horarioValido,times(0)).valido();
	}
	
}
