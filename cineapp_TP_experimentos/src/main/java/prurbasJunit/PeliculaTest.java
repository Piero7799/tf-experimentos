package prurbasJunit;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.itinajero.app.model.Pelicula;

public class PeliculaTest {
	Pelicula pelicula;
	Date utilDate;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		pelicula = new Pelicula();
		utilDate = new Date();
		
		pelicula.setTitulo("df");
		pelicula.setDuracion(200);
		pelicula.setGenero("Drama");
		pelicula.setClasificacion("B");
		pelicula.setFechaEstreno(utilDate);
		pelicula.setEstatus("Activa");
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDuracion() {
		System.out.println("----------------------");
		System.out.println("Inicio prueba : DURACION de una pelicula");
		int resultado =pelicula.getDuracion();
		System.out.println("Valor DURACION:" + pelicula.getDuracion());
		int resultadoEsperado = 240;
		assertTrue("Dato ingresado no valido, duracion excedida",resultado <= resultadoEsperado);
		System.out.println("Fin prueba : DURACION de una pelicula");
		System.out.println("----------------------");
	}
	
	@Test
	public void testGetTitulo() {
		System.out.println("**************************");
		System.out.println("Inicio prueba : Validacion TITULO Pelicula");
		String resultado = pelicula.getTitulo(); 
		System.out.println("TITULO A EVALUAR:" + pelicula.getTitulo());
		assertTrue("Sin titulo!",resultado != "");
		System.out.println("Fin prueba : Validacion TITULO Pelicula");
		System.out.println("**************************");
	}
	
	
	
}
