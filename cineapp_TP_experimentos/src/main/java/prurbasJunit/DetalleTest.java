package prurbasJunit;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.itinajero.app.model.Detalle;
import net.itinajero.app.model.Pelicula;

public class DetalleTest {
	//Pelicula pelicula;
	Date utilDate;
	Detalle detallePelicula;
	URL u ;
	int code;
	HttpURLConnection huc;
	URLValidator validar;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		detallePelicula = new Detalle();
		
		utilDate = new Date();
		
		detallePelicula.setDirector("DirectorPrueba");
		detallePelicula.setActores("ActorPrueba");
		detallePelicula.setTrailer("https://www.hastalavistabeibi.com");
		validar = new URLValidator();
		u = new URL (detallePelicula.getTrailer());
		
	}

	@Test
	public void testGetTrailer() throws IOException {
		System.out.println("**************************");
		System.out.println("Inicio prueba : URL VALIDO");

		huc = ( HttpURLConnection )  u.openConnection (); 
		huc.setRequestMethod("GET");  
		huc.connect(); 
		int resultado = huc.getResponseCode();
		System.out.println("URL a evaluar : " + detallePelicula.getTrailer());
		System.out.println("Resultado: " +resultado);
		assertTrue("Funcionando",resultado != 404);	

		System.out.println("Fin prueba : URL VALIDO");
		System.out.println("**************************");
	}




}
